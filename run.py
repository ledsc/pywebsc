import asyncio
import logging
import random

from websc_client import WebSClientAsync as WebSClient
from websc_client import WebSCAsync


async def on_device_change(data: dict[str, int]):
    # Display changed variables
    print(data)


async def run():
    # Connect to WebSC server
    client = WebSClient(host="127.0.0.1", port=8443)
    await client.connect()

    # Start observer for API listening
    observer = asyncio.create_task(client.observer())

    # Selects a device at random from the available devices.
    device: WebSCAsync = random.choice(list(client.devices.values()))

    # Registration for a device state change event
    device.set_callback(on_device_change)

    try:
        while True:
            # It changes colour randomly once every half a second
            await device.set_rgb(
                red=round(random.random() * 255),
                green=round(random.random() * 255),
                blue=round(random.random() * 255),
            )
            await asyncio.sleep(0.5)
    finally:
        observer.cancel()
        await client.disconnect()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    try:
        asyncio.run(run())
    except KeyboardInterrupt:
        pass
